/*Дан обьект `employee`. Добавьте в него свойства age и salary, 
не изменяя изначальный объект (должен быть создан новый объект, 
который будет включать все необходимые свойства). 
Выведите новосозданный объект в консоль.
*/
const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

function createObject({ name, surname, age = 40, salary = 35000 }) {
  const user = {
    name: name,
    surname: surname,
    age: age,
    salary: salary,
  };
  return user;
}
console.log(createObject(employee));
