// Дополните код так, чтоб он был рабочим

// ```javascript
const array = ["value", () => "showValue"];

// Допишите ваш код здесь
const [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue()); // должно быть выведено 'showValue'

// console.log(showValue); // () => "showValue"
