//У нас есть объект `user`:
const user1 = {
  name: "John",
  years: 30,
  //   isAdmin: true,
};

/*
Напишите деструктурирующее присваивание, которое:
 - свойство name присвоит в переменную name
 - свойство years присвоит в переменную age
 - свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
 
Выведите переменные на экран.
*/
function createNewUser({ name, years, isAdmin = false }) {
  const user = {
    name,
    years,
    isAdmin,
  };
  return user;
}
const newUser = createNewUser(user1);
console.log(newUser);
// document.write(`Object user = ${JSON.stringify(newUser)} `);
// document.write(`Object user = ${Object.entries(newUser)}`);
document.write(
  `name: ${newUser.name}, years: ${newUser.years}, isAdmin: ${newUser.isAdmin}.`
);
